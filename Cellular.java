/*
 * Lim, John Voltaire
 * 20 January 2017
 * Amaysim Shopping Cart Exercise
 */
package cellular;

import java.util.ArrayList;
import java.util.Collections;

/*
* 
* Enum Definition for Products
*
*/
enum Mobile {
    ITEM01( "ult_small",    "Unlimited 1GB",    24.9),
    ITEM02( "ult_medium",   "Unlimited 2GB",    29.9),
    ITEM03( "ult_large",    "Unlimited 5GB",    44.9),
    ITEM04( "1GB",          "1GB Data Pack",    9.9);
    
    String productCode;
    String productName;
    double productPrice;
    
    Mobile(String code, String name, double price) {
        productCode = code;
        productName = name;
        productPrice = price;
    }
}

/*
* 
* Shopping Cart
*
*/
public class Cellular {
    
    static final String promo_code = "I<3AMAYSIM";
    double total;
    
    static Mobile item1 = Mobile.ITEM01;
    static Mobile item2 = Mobile.ITEM02;
    static Mobile item3 = Mobile.ITEM03;
    static Mobile item4 = Mobile.ITEM04;
    static Promotion[] promoOffers;
    
    //ArrayList<Mobile> cart = new ArrayList<Mobile>();
    ArrayList<String> cart = new ArrayList<String>();
    
    
    Cellular () {
        total = 0;
    }
        
    double getTotal() {
        return round(total,2);
    }
    
    void init() {
        total = 0;
        
        ArrayList<String> shop = new ArrayList<String>();
        
        cart = shop;
        
        Promotion[] promo = {
            new Promotion("I<3AMAYSIM",     "10% Discount Promo",   0.1,    true),
            new Promotion("ult_small_free", "Unlimited 1GB",        -24.9,  true),
            new Promotion("ult_large_disc", "Unlimited 5GB",        39.9,   true)
        };
        
        promoOffers = promo;
    }
    
    void add(Mobile item) {        
        switch(item) {
            case ITEM01: 
                cart.add(item.ITEM01.productCode);
                break;
            case ITEM02:
                cart.add(item.ITEM02.productCode);
                break;
            case ITEM03:
                cart.add(item.ITEM03.productCode);
                break;
            case ITEM04:
                cart.add(item.ITEM04.productCode);
                break;
        }
    }
    
    void add(Mobile item, String offer) {
        
        add (item);
        
        //Check if Coupon Code have already been entered
        if (!cart.contains(offer)) {
            if (offer == promoOffers[0].promoCode) {
                //System.out.println("Promo " + offer + " exists");
                if (promoOffers[0].status) {
                    //System.out.println("Promo " + offer + " is active");
                    cart.add(promoOffers[0].promoCode);
                }
                else {
                    //System.out.println("Promo " + offer + " is not active");
                }
            }
        }
        else {
            System.out.println("Coupon already exists at index[" + 
                    cart.lastIndexOf(offer) + "]");
        }
    }
    
    void items() {
        double total = 0;
        int quantity = 0;
        int offer;
        
        //1GB Data Pack
        quantity = Collections.frequency(cart, item4.productCode);
        //System.out.println("Frequency 1GB Data Pack: " + quantity);
        if (quantity > 0) {
            total = total + (quantity * item4.productPrice);
            System.out.println(quantity + " x " + item4.productName + 
                    " = " + (quantity * item4.productPrice));
        }
        
        //3 for 2 deal - 1GB Unlimited Sim
        quantity = Collections.frequency(cart, item1.productCode);
        //System.out.println("Frequency 1GB Unlimited: " + quantity);
        if (quantity >= 1 && quantity <= 2) {
            total = total + (quantity * item1.productPrice);
            System.out.println(quantity + " x " + item1.productName + 
                    " = " + (quantity * item1.productPrice));
        }
        else if (quantity >= 3) {
            if (promoOffers[1].status) {
                offer = quantity / 3;
                System.out.println("You have free " + offer + " " + 
                        item1.productName);
                total = total + ((quantity * item1.productPrice) + 
                        (offer * promoOffers[1].promotionalPrice));
                System.out.println(quantity + " x " + item1.productName + 
                        " = " + ( (quantity * item1.productPrice) + 
                        (offer * promoOffers[1].promotionalPrice) ) );
            }
            else {
                total = total + ((quantity * item1.productPrice));
                System.out.println(quantity + " x " + item1.productName + 
                        " = " + (quantity * item1.productPrice) );
            }
        }

        //Free 1GB Data Pack for every 2GB Unlimited Sim
        quantity = Collections.frequency(cart, item2.productCode);
        //System.out.println("Frequency 2GB Unlimited: " + quantity);
        if (quantity > 0) {
            total = total + (quantity * item2.productPrice);
            System.out.println(quantity + " x " + item2.productName + 
                    " = " + item2.productPrice);
            //Consider checking active/existing offer
            System.out.println(quantity + " x " + item4.productName);
        }

        //Discount - 5GB Unlimited Sim
        quantity = Collections.frequency(cart, item3.productCode);
        //System.out.println("Frequency 5GB Unlimited: " + quantity);
        if (quantity >= 1 && quantity <= 3) {
            System.out.println(quantity + " x " + item3.productName + 
                    " = " + (quantity * item3.productPrice) );
            total = total + (quantity * item3.productPrice);
        }
        else if (quantity > 3) {
            if (promoOffers[2].status) {
                System.out.println(quantity + " x " + item3.productName + 
                        " = " + (quantity * promoOffers[2].promotionalPrice) );
                total = total + (quantity * promoOffers[2].promotionalPrice);
            }
            else {
                System.out.println(quantity + " x " + item3.productName + 
                        " = " + (quantity * item3.productPrice) );
                total = total + (quantity * item3.productPrice);
            }
        }
        
        //Coupon Code - I<3AMAYSIM
        quantity = Collections.frequency(cart, promo_code);
        if (quantity == 1 && promoOffers[0].status) {
            System.out.println("--- " + promoOffers[0].promoCode +  
                    " Promo Applied ---");
            System.out.println("Sub-total: " + total);
            total = total - (total * promoOffers[0].promotionalPrice);
            System.out.println("10% Discount: " + 
                    (total * promoOffers[0].promotionalPrice));
        }
        
        //this.total = round(total, 2);
        this.total = total;
    }
    
    void total() { 
        if (total == 0) {
            items();
        }
        
        System.out.println("Expected Cart Total: " + getTotal()+ "\n\n");
    }
    
    /*
    * 
    * http://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-
    *
    */
    double round (double value, int places) {
        long factor = (long) Math.pow(10,places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cellular cell = new Cellular();
        cell.init();
        
        cell.add(item1, promo_code);
        cell.add(item1, promo_code);
        //cell.add(item3);
        //cell.add(item1);
        
        //cell.add(item2);
        //cell.add(item2, promoOffers[0].promoCode);
        //cell.add(item2, promo_code);
        cell.add(item4);
        
        //cell.add(item3);
        //cell.add(item3);
        //cell.add(item3);      
        //cell.add(item1);
        //cell.add(item1);
        cell.items();
        cell.total();
        
        cell.init();
        cell.add(item1);
        cell.add(item1);
        cell.add(item1);
        cell.add(item3);
        cell.items();
        cell.total();
        
        cell.init();
        cell.add(item1);
        cell.add(item1);
        cell.add(item3);
        cell.add(item3);
        cell.add(item3);
        cell.add(item3);
        cell.items();
        cell.total();
            
        cell.init();
        cell.add(item1);
        cell.add(item2);
        cell.add(item2);
        cell.items();
        cell.total();
        
        cell.init();
        cell.add(item1, promo_code);
        cell.add(item4);
        cell.items();
        cell.total();
    }
}

/*
* 
* Class Definition for Promotional Offers
*
*/
class Promotion {
    String promoCode;
    String promoName;
    double promotionalPrice;
    Boolean status;

    Promotion (String code, String name, double price, Boolean active) {
        promoCode = code;
        promoName = name;
        promotionalPrice = price;
        status = active;
    }
}
